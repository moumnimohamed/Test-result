import React, {useEffect, useRef, useState} from "react"

import styled from 'styled-components';
import {Icon} from "../GlobalStyles/styles";
import playIcon from "../images/play.png";
import pauseIcon from "../images/pause.png";


const Video =(props)=>{

    const {videoLink ,poster}=props

    const [play,setPlay]=useState(false)
    const videoRef = useRef(null);

    useEffect(()=>{
            if (play) {
                videoRef.current.play();
            } else {
                videoRef.current.pause();
            }
    },[play])

    return(
        <Container>
            <video ref={videoRef} poster={poster}   style={{objectFit:"cover",borderRadius:10}} width="100%" height="100%"  >
                <source src={videoLink} type="video/mp4"/>
                <source src={videoLink} type="video/ogg"/>
            </video>
            <BtnContainer>
                <PlayButton isPlaying={play} onClick={()=>setPlay(!play) }>
                    <Icon src={ play ? pauseIcon : playIcon}  alt={"dd"} />
                </PlayButton>
            </BtnContainer>
        </Container>
    )
}


const Container = styled.div`
  width:100%;
  cursor: pointer;
  height:100%;
   overflow:hidden;
position:relative
`;

const PlayButton = styled.button`
  padding: 16px ;
background-color:rgb(151, 156, 157,0.7);
border:0;
display:flex;
cursor: pointer;
border-radius:100%;
transition: all .2s ease-in-out;
   opacity: ${props => `   ${props.isPlaying ? 0 : 1 } `};

  &:hover {
         opacity:  1;
     background-color:rgb(151, 156, 157,0.9);
     transform: scale(1.5);
  }
`;

const BtnContainer = styled.div`
position: absolute;
 top: 50%;
 left: 50%;
 transform: translate(-50%, -50%);
  
`;




export  default Video
