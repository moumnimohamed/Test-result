import React from "react"

import styled from 'styled-components';



const TalentCard =(props)=>{
    const {talent,handleOnClick}=props
    return(
        <Container onClick={handleOnClick}>
               <ImageContainer>
                <Image img={talent?.user?.avatar_url} alt={talent?.name_en}/>
               </ImageContainer>
            <H3>{talent?.name_en}</H3>

            <Card >
                    {talent?.tags?.slice(0, 3).map((tag,i)=> {return <H4 key={i}>{tag?.name_en},</H4>})}
                </Card>
        </Container>
    )
}


const Container = styled.div`
  width:100%;
  
  cursor: pointer;
  height:297px;
   overflow:hidden;
   transition: transform 500ms ease;
  &:hover{
   transform :  scale(1.1);
   }
   

`;

const Card = styled.div`
   display:flex;
   flex-wrap: wrap;
`;

const H4 = styled.h4`
margin: 0;
    font-size: 9px;
    font-weight:bold;
    color:#BCC5CF;
    margin-right:5px
`;
const H3 = styled.h3`
margin-Bottom:5px;
text-align: left;
margin: 0;
    font-size: 19px;
    font-weight:bold;
    color:#BCC5CF;
    margin-right:5px
`;
const ImageContainer = styled.div`
overflow:hidden;
margin-bottom:5px;
border-radius:10px;
`
const Image = styled.div`

  background-image: ${props => ` url( ${props.img} )`};
 height: 250px; 
  background-position: center;  
  background-repeat: no-repeat;  
  background-size: cover;
  
`;

export  default TalentCard
