import React, {useEffect} from "react"
import TalentCard from "../../components/TalentCard";
import {Container, H2, Icon, IconButton, ListContainer, Pagination, Title} from "../../GlobalStyles/styles";
import rArrow from "../../images/r-arrow.png"
import lArrow from "../../images/l-arrow.png"
import Indicator from "../../components/Indicator";
import useFetch from "../../helpers/useFetch";


const Index =(props)=>{

     const {history}=props

    const [{response,isLoading,page,setPage,maxPages},doFetch]=useFetch()

     useEffect(()=>{
         doFetch(`https://stg.starzly.io/api/talents?per_page=16&page=${page}`)
     },[page])



    return(
        <Container>
            <H2 >All TV</H2>
            <ListContainer>
                {isLoading ?
                    <Indicator/>
                :
                    response?.map((talent,i)=> {
                        return  <TalentCard   key={i}
                                              talent={talent}
                                              handleOnClick={()=> history.push("/talentDetails",{talent})}
                        />
                    })
                }
            </ListContainer>
            <Pagination>
                <IconButton onClick={page > 1 ? ()=>  setPage(page-1) : null } type="button">
                    <Icon src={lArrow}  alt={"dd"} />
                </IconButton>
                <Title>{`Page ${page} of ${maxPages}`}</Title>
                <IconButton  onClick={page < maxPages ? ()=>  setPage(page+1) : null } type="button"><Icon src={rArrow}  alt={"dd"} /></IconButton>
            </Pagination>
        </Container>
    )
}

export  default Index
