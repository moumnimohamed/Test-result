import styled from "styled-components";

export const Container = styled.div`
    padding: 0 20%;
    margin-top:32px;
     @media (max-width: ${1440}px) {
           padding: 0 10%;
    } 
      
      @media (max-width: ${768}px) {
           padding: 0 5%;
    }  
 

`;

export const Details = styled.div`
  display: flex; 
  flex-direction: row;
  
   @media (max-width: ${768}px) {
   margin-top:10px;
            flex-direction: column;
            justify-content: center;
    align-items: center;
    display: flex;
    }  
`;
export const VideoContainer = styled.div`
  height: 512px;
   width: 310px;
   
     @media (max-width: ${768}px) {
              width: 100%;
         height: 70%;

    }
`;

export const DataContainer = styled.div`
 flex: 1;
   text-align: left; 
   padding: 0 16px
`;
export const H2 = styled.h2`
      color:#FF14BD;
      font-size:2rem;
      margin: 6px 0;
`;
export const H3 = styled.h3`
   
      color:  ${props => `  ${props.color || "#5C5C5C"} `} ;
      font-size:  ${props => `  ${props.fontSize || "1rem"} `} ;
       
      margin-top: 6px  ;
      margin-bottom: 16px  ;
`;

export const StarsContainer = styled.div`
display: flex;
    justify-content: center;
    align-items: center;
   `
export const BigBadge = styled.div`
      flex: 1;
      display:flex;
  background-color: #F5F5F5;
   justify-content:center;
   align-items:flex-end; 
    padding:  16px 0;
    color:#C0C7CF;
      border-radius:4px

`;
export const BigSpan = styled.span`
margin-left:4px;
    color:#BBC2CC;
font-Weight:bold
`
export const MiniIcon = styled.img`
margin-right:6px;
    width: 16px;
`

export const StyledP = styled.p`
color:#AEAEAE
`
export const H5 = styled.h5`
color:#AEAEAE;
margin:0
`

export const Badges = styled.div`
display: flex;
    flex: 1;
`
export const Badge = styled.div`
      display:flex;
      margin-right:10px;
  background-color: #F5F5F5;
   justify-content:center;
   align-items:flex-end; 
    padding:  5px 10px;
    border-radius:4px;
    color:#C0C7CF;
`;

export const Line = styled.div`
  background-color: #F5F5F5;
    height:1px;
    display:flex;
    flex:1;
    margin:16px 0;
`
export const Part2 = styled.div`
 flex: 1;
 display:flex;
`
export const ButtonContainer = styled.div`
         margin:16px 0;
`
export const BigButton = styled.button`
width: 100%;
  background-color: #F81C78;
   justify-content:center;
   align-items:flex-end; 
    padding:  16px 0;
    color:#fff;
    border:0;
    border-radius:4px;
    font-weight:bold;
    cursor:pointer;
    &:hover{
    opacity:.9
    }
`

export const ReviewContainer = styled.div`
   justify-content: space-between;
    align-items: center;
    display:flex;
`
export const CardsContainer = styled.div`
    display: flex;
    overflow-x: auto;
    &::-webkit-scrollbar{
    display:none
    }
`

export const CardParent = styled.div`
    min-width:10rem;
    overflow:visible;
        margin-top:16px;

    margin-left:16px
`


