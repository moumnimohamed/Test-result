import React, {useEffect} from "react"

import Video from "../../components/Video"
import Indicator from "../../components/Indicator";
import clock from "../../images/future.png";
import star from "../../images/star.png";
import star2 from "../../images/star2.png";
import {
    Badge,
    Badges,
    BigBadge,
    BigButton,
    BigSpan,
    ButtonContainer,
    CardParent,
    CardsContainer,
    Container,
    DataContainer,
    Details,
    H2,
    H3,
    H5,
    Line,
    MiniIcon,
    ReviewContainer,
    StarsContainer,
    StyledP,
    VideoContainer
} from "./styles";
import TalentCard from "../../components/TalentCard";
import useFetch from "../../helpers/useFetch";


const starsList=[
    {user:{avatar_url:"https://stgmedia.starzly.io/photos/267/8/2020/phpHZe2ai.png ",name_en:"Talent name"}},
    {user:{avatar_url:"https://stgmedia.starzly.io/photos/268/8/2020/phpLdupks.png",name_en:"Talent name"}},
    {user:{avatar_url:"https://stgmedia.starzly.io/photos/271/8/2020/phpJrOOVb.png",name_en:"Talent name"}},
    {user:{avatar_url:"https://stgmedia.starzly.io/photos/276/8/2020/phpmFb95x.png ",name_en:"Talent name"}},
    {user:{avatar_url:"https://stgmedia.starzly.io/photos/277/8/2020/php6rTAqR.png",name_en:"Talent name"}},
    {user:{avatar_url:"https://stgmedia.starzly.io/photos/281/8/2020/php9G1Nft.png ",name_en:"Talent name"}},
    {user:{avatar_url:"https://stgmedia.starzly.io/photos/283/8/2020/phpUNmt1h.png",name_en:"Talent name"}},
    {user:{avatar_url:"https://stgmedia.starzly.io/photos/267/8/2020/phpHZe2ai.png ",name_en:"Talent name"}},
    {user:{avatar_url:"https://stgmedia.starzly.io/photos/267/8/2020/phpHZe2ai.png ",name_en:"Talent name"}},
    {user:{avatar_url:"https://stgmedia.starzly.io/photos/267/8/2020/phpHZe2ai.png ",name_en:"Talent name"}},
    {user:{avatar_url:"https://stgmedia.starzly.io/photos/267/8/2020/phpHZe2ai.png ",name_en:"Talent name"}},
]

const TalentDetail =(props)=>{
     const {history}=props

    const talentId=history?.location?.state?.talent?.id

    const [{response,isLoading,page},doFetch]=useFetch()

    useEffect(()=>{
        doFetch(`https://stg.starzly.io/api/talents/${talentId}`)
    },[page])


    return(
        <Container>
            {isLoading ?
                <Indicator/>
                :
                <>
                <Details>
                    <VideoContainer>
                        {response?.user?.intro_video_url &&
                        <Video
                            videoLink={response?.user?.intro_video_url}
                            poster={response?.user?.intro_video_thumbnail_url}
                        />
                        }

                    </VideoContainer>
                    <DataContainer>
                        <H2>{response?.name_en}</H2>
                        <H3>{response?.bio_en}</H3>
                        <BigBadge>
                            <MiniIcon src={clock} alt={"clock icon"}/>
                            typically respond in <BigSpan>2 days</BigSpan>
                        </BigBadge>
                        <StyledP>
                            Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du
                        </StyledP>
                        <Badges>
                            {response?.tags?.map((tag,i)=> {
                                return <Badge key={i} >
                                    {tag?.name_en}
                                </Badge>
                            })}
                        </Badges>
                        <Line/>
                        <ReviewContainer>
                            <StarsContainer>
                                <MiniIcon src={star} alt={"rate icon"}/>
                                <MiniIcon src={star} alt={"rate icon"}/>
                                <MiniIcon src={star} alt={"rate icon"}/>
                                <MiniIcon src={star2} alt={"rate icon"}/>
                                <MiniIcon src={star2} alt={"rate icon"}/>
                                <BigSpan>4.0</BigSpan>
                            </StarsContainer>
                            <H5>View all 36 reviews</H5>
                        </ReviewContainer>
                    </DataContainer>
                </Details>
                      <ButtonContainer>
                          <BigButton>
                              Book now -  {response?.converted_currency} {response?.converted_cost}
                          </BigButton>
                      </ButtonContainer>

                <ReviewContainer>
                    <H3 fontSize={"1.5rem"} color={"#A5B0BD"}>Previous videos</H3>
                    <BigSpan>View all</BigSpan>
                    </ReviewContainer>
                    <CardsContainer>
                        {starsList.map((e,i)=>{
                            return <CardParent  >
                                <TalentCard   key={i}
                                              talent={e}

                                />
                            </CardParent>

                        })}
                    </CardsContainer>


                </>
            }
            </Container>
    )
}

export  default TalentDetail
