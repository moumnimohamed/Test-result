import styled from "styled-components";

export const Container = styled.div`
  padding:16px
`;

export const ListContainer = styled.div`
  display: grid;
  grid-template-columns:repeat(auto-fit,minmax(10rem,1fr));
  gap: 1rem;
`;
export const NoResult = styled.div`
  display: flex;
  justify-content:center;
  align-items:center;
  min-height:60vh
  
`;
export const Pagination = styled.div`
margin-top: 32px;
  display: flex;
   justify-content:center;
   align-items:center;
`;

export const IconButton = styled.button`
padding: 16px ;
background-color:#F5F5F5;
border:0;
display:flex;
cursor: pointer;
border-radius:5px;
  &:hover {
     background-color:#eee;
  }
`;

export const Title = styled.h4`
color:#CCD3DA;
margin: 0 16px
  
`;

export const Icon = styled.img`
width: 12px;
    height: 12px;
 
`;


export const H2 = styled.h2`
  color:#ddd;
  text-align:left;
`;
