import { useState, useEffect } from 'react';
import axios from "axios";

const useFetch = () => {
    const [response, setResponse] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);
    const [page,setPage]=useState(1)
    const [url,setUrl]=useState("")
    const [maxPages,setMaxPages]=useState(0)

    const doFetch=( url)=>{
        setUrl(url)
            setIsLoading(true)
    }

    useEffect(()=>{
        if(!isLoading){
            return null
        }

        const getDAta = async () => {
            setIsLoading(true)
            try {
                let response = await axios.get(url)
                response = await response
                setIsLoading(false)

                setResponse(  response?.data?.data || response?.data)
                if(response?.data?.last_page)  {
                    setMaxPages(response?.data?.last_page)
                }

            }
            catch (e) {
                setIsLoading(false)
                alert(e);
                setError(e)
            }
        }
        getDAta()
    },[isLoading])

    return [{response,error,isLoading,page,setPage,maxPages},doFetch];
};

export default useFetch;
