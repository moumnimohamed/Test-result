import './App.css';
import React from "react"
import {BrowserRouter as Router  ,Route} from "react-router-dom"
 import TalentsList from "./pages/TalentsList"
 import TalentDetails from "./pages/TalentDetails"
function App() {






  return (
    <div className="App">

             <Router>
               <Route exact path={"/"}  component={TalentsList}/>
               <Route path={"/talentDetails"}  component={TalentDetails}/>
             </Router>
    </div>
  );
}

export default App;
